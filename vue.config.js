module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    proxy: {
      '/client': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/users': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/logout': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/private': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/oauth2/authorization/login-client': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },
      '/login/oauth2/code/login-client': {
        target: 'http://40.114.33.245:8081',
        ws:true,
        changeOrigin: true,
        xfwd: true
      },

    }
  },
};